
/**
 * 
 *     SAHUARO WATER CLIENT
 *          07/25/20 
 *            V1
 * Caracteristicas:
 * 1. El led indica el valor de la moneda
 * insertada de acuerdo al numero de blinks
 * 2. Manda mensaje al servidor que se inserto una 
 * moneda de tal denominacion
 * 3. Manda mensaje por el puerto serie que se inserto 
 * una moneda
 * 4. Modulo ESP32
 * 5. No OTA
 * 6. NO WIFIMANAGER
*/

#include <Arduino.h>
//#include <WiFi.h>
#include <PubSubClient.h>

#define ESP32
#if !( defined(ESP8266) || defined(ESP32) )
  #error This code is intended to run only on the ESP8266 and ESP32 boards ! Please check your Tools->Board setting.
#endif

// Use from 0 to 4. Higher number, more debugging messages and memory usage.
#define _WIFIMGR_LOGLEVEL_    3

//For ESP32, To use ESP32 Dev Module, QIO, Flash 4MB/80MHz, Upload 921600

//Ported to ESP32
#ifdef ESP32
  #include <esp_wifi.h>
  #include <WiFi.h>
  #include <WiFiClient.h>
  
  #define ESP_getChipId()   ((uint32_t)ESP.getEfuseMac())
  
  #define LED_ON      HIGH
  #define LED_OFF     LOW
#else
  #include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
  //needed for library
  #include <DNSServer.h>
  #include <ESP8266WebServer.h>
  
  #define ESP_getChipId()   (ESP.getChipId())
  
  #define LED_ON      LOW
  #define LED_OFF     HIGH
#endif

// SSID and PW for Config Portal
String ssid = "ESP_" + String(ESP_getChipId(), HEX);
const char* password = "carlos123654";

// SSID and PW for your Router
String Router_SSID;
String Router_Pass;

// Use true for dynamic DHCP IP, false to use static IP and  you have to change the IP accordingly to your network
#define USE_DHCP_IP     true

#if USE_DHCP_IP
  // Use DHCP
  IPAddress stationIP   = IPAddress(0, 0, 0, 0);
  IPAddress gatewayIP   = IPAddress(192, 168, 2, 1);
  IPAddress netMask     = IPAddress(255, 255, 255, 0);
#else
  // Use static IP
  IPAddress stationIP   = IPAddress(192, 168, 2, 232);
  IPAddress gatewayIP   = IPAddress(192, 168, 2, 1);
  IPAddress netMask     = IPAddress(255, 255, 255, 0);
#endif

IPAddress dns1IP      = gatewayIP;
IPAddress dns2IP      = IPAddress(8, 8, 8, 8);

// Use false if you don't like to display Available Pages in Information Page of Config Portal
// Comment out or use true to display Available Pages in Information Page of Config Portal
// Must be placed before #include <ESP_WiFiManager.h>
#define USE_AVAILABLE_PAGES     false

// Use false to disable NTP config
#define USE_ESP_WIFIMANAGER_NTP     true

// Use true to enable CloudFlare NTP service. System can hang if you don't have Internet access while accessing CloudFlare
// See Issue #21: CloudFlare link in the default portal => https://github.com/khoih-prog/ESP_WiFiManager/issues/21
#define USE_CLOUDFLARE_NTP          false

#include <ESP_WiFiManager.h>              //https://github.com/khoih-prog/ESP_WiFiManager

// Onboard LED I/O pin on NodeMCU board
//const int PIN_LED = 2; // D4 on NodeMCU and WeMos. GPIO2/ADC12 of ESP32. Controls the onboard LED.




////////////////////////////
////////ORIGINAL MQTT SAHUARO WATER
//const char* ssid = "hola";
//const char* password = "jashimotoyesme";

///Credenciales Sahuaro Water
const char *mqtt_server = "cursoiotspimj.ml";
const int   mqtt_port = 1883;
const char *mqtt_user = "shuarowater";
const char *mqtt_pass = "carlos72520";

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
char msg[25];

byte coinInserted = 0;
int coinValue = 0;
 
byte debounce =0;
/* pin that is attached to interrupt */
byte interruptPin = 12;
/* hold the state of LED when toggling */
volatile byte state = LOW;

///////////******
/////////// PROT DE FUNCIONES
void setup_wifi();
void callback(char* topic, byte* payload, unsigned int length);
void reconnect();
void coin_ch926();


void setup() {
///////WIFI MANAGER
  // put your setup code here, to run once:
  // initialize the LED digital pin as an output.
 // pinMode(PIN_LED, OUTPUT);
  
 // Serial.begin(115200);
 // while (!Serial);
  
 // Serial.println("\nStarting ConfigOnStartup on " + String(ARDUINO_BOARD));
pinMode(BUILTIN_LED, OUTPUT);
digitalWrite(BUILTIN_LED, 1);
  unsigned long startedAt = millis();

 // digitalWrite(PIN_LED, LED_ON); // turn the LED on by making the voltage LOW to tell us we are in configuration mode.

  //Local intialization. Once its business is done, there is no need to keep it around
  // Use this to default DHCP hostname to ESP8266-XXXXXX or ESP32-XXXXXX
  ESP_WiFiManager ESP_wifiManager;
  // Use this to personalize DHCP hostname (RFC952 conformed)
  //ESP_WiFiManager ESP_wifiManager("ConfigOnStartup");

  //set custom ip for portal
  //ESP_wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 100, 1), IPAddress(192, 168, 100, 1), IPAddress(255, 255, 255, 0));

  ESP_wifiManager.setMinimumSignalQuality(-1);
  
  // Set static IP, Gateway, Subnetmask, DNS1 and DNS2. New in v1.0.5
  ESP_wifiManager.setSTAStaticIPConfig(stationIP, gatewayIP, netMask, dns1IP, dns2IP);   

  // We can't use WiFi.SSID() in ESP32as it's only valid after connected.
  // SSID and Password stored in ESP32 wifi_ap_record_t and wifi_config_t are also cleared in reboot
  // Have to create a new function to store in EEPROM/SPIFFS for this purpose
  Router_SSID = ESP_wifiManager.WiFi_SSID();
  Router_Pass = ESP_wifiManager.WiFi_Pass();

  //Remove this line if you do not want to see WiFi password printed
  //Serial.println("Stored: SSID = " + Router_SSID + ", Pass = " + Router_Pass);

  //Check if there is stored WiFi router/password credentials.
  //If not found, device will remain in configuration mode until switched off via webserver.
  //Serial.print("Opening configuration portal.");

  if (Router_SSID != "")
  {
    ESP_wifiManager.setConfigPortalTimeout(120); //If no access point name has been previously entered disable timeout.
  //  Serial.println("Timeout 120s");
  }
 // else
   // Serial.println("No timeout");

  // SSID to uppercase
  ssid.toUpperCase();

  //it starts an access point
  //and goes into a blocking loop awaiting configuration
  ESP_wifiManager.startConfigPortal((const char *) ssid.c_str(), password);
    //Serial.println("Not connected to WiFi but continuing anyway.");
  //else
    //Serial.println("WiFi connected...yeey :)");

  //digitalWrite(PIN_LED, LED_OFF); // Turn led off as we are not in configuration mode.

  // For some unknown reason webserver can only be started once per boot up
  // so webserver can not be used again in the sketch.
#define WIFI_CONNECT_TIMEOUT        30000L
#define WHILE_LOOP_DELAY            200L
#define WHILE_LOOP_STEPS            (WIFI_CONNECT_TIMEOUT / ( 3 * WHILE_LOOP_DELAY ))

  startedAt = millis();

  while ( (WiFi.status() != WL_CONNECTED) && (millis() - startedAt < WIFI_CONNECT_TIMEOUT ) )
  {
#ifdef ESP32
    WiFi.mode(WIFI_STA);
    WiFi.persistent (true);
    // We start by connecting to a WiFi network

   // Serial.print("Connecting to ");
   // Serial.println(Router_SSID);

    WiFi.config(stationIP, gatewayIP, netMask);
    //WiFi.config(stationIP, gatewayIP, netMask, dns1IP, dns2IP);
    
    WiFi.begin(Router_SSID.c_str(), Router_Pass.c_str());
#endif

    int i = 0;
    while ((!WiFi.status() || WiFi.status() >= WL_DISCONNECTED) && i++ < WHILE_LOOP_STEPS)
    {
      delay(WHILE_LOOP_DELAY);
    }
  }

  //Serial.print("After waiting ");
  //Serial.print((millis() - startedAt) / 1000);
  //Serial.print(" secs more in setup(), connection result is ");

  //if (WiFi.status() == WL_CONNECTED)
 // {
    //Serial.print("connected. Local IP: ");
    //Serial.println(WiFi.localIP());
  //}


  /////////////////////////
  /////MQTT CONFIGURATION
  
  /* set the interrupt pin as input pullup*/
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), coin_ch926, HIGH);
  // Apaga el LED
  state = 0;
  digitalWrite(BUILTIN_LED, state);
  Serial.begin(9600);
  randomSeed(micros());
 // setup_wifi();
  client.setServer(mqtt_server,mqtt_port);
  client.setCallback(callback);
}

void loop() {
  if(!client.connected()){
    reconnect();
  }

client.loop();

    if(coinInserted == 1){
        if(digitalRead(interruptPin)==0)
          debounce = 0;

        long now = millis();
        if(now - lastMsg > 500){
          digitalWrite(BUILTIN_LED, HIGH);
        // lastMsg = now;
        // temp1++;
        // temp2++;
        // volts++;

        // String to_send = String(temp1)+ "," + String(temp2)+ "," + String(volts);
        // to_send.toCharArray(msg,25);
        //  Serial.print("Publicamos mensaje ->");
        //  Serial.println(msg);
          String to_send = String(coinValue);
          to_send.toCharArray(msg,25);
          Serial.print("Publicamos mensaje -> ");
          Serial.println(msg);
          client.publish("/sahuarowater/suc1/caja1/coins",msg);
          coinInserted = 0;
          coinValue=0;
          state = 0;
          digitalWrite(BUILTIN_LED, state);
        }
    }
}


//*********************************
//**** CONEXION WIFI
//**********************************
/*
void setup_wifi(){
  delay(10);
  
  Serial.println();
  Serial.println("Conectado a ");
  Serial.println(ssid);
  
  WiFi.begin(ssid,password);

  while(WiFi.status()!= WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Conectado a red WIFI!");
  Serial.println("Direccion IP: ");
  Serial.println(WiFi.localIP());

}
*/
void callback(char* topic, byte* payload, unsigned int length){
  String incoming = "";
  Serial.print("Mensaje recibido desde -> ");
  Serial.print(topic);
  Serial.println("");

  for(int i = 0; i < length ; i++){
    incoming += (char)payload[i];
  }
  incoming.trim();
  Serial.println("Mensaje " + incoming);

  if(incoming == "on"){
    digitalWrite(BUILTIN_LED, HIGH);
  }
  else{
    digitalWrite(BUILTIN_LED, LOW);
  }

}

void reconnect(){
  while(!client.connected()){
    Serial.print("Intentando conexion MQTT..");
    String clientId = "Esp32";
    clientId += String(random(0xffff),HEX);
    
    if(client.connect(clientId.c_str(),mqtt_user,mqtt_pass)){
      Serial.println("Conectado!");
      ////Nos suscribimos 
      client.subscribe("led1");

    }else
    {
      Serial.print("fallo :( con error -> ");
      Serial.print(client.state());
      Serial.println("Intentando de nuevo en 5 segundos");
      delay(5000);
    }
    

  }

}

//*********************************
/* interrupt function toggle the LED */
//*********************************
void coin_ch926() {
  if(debounce == 0){
      state = !state;
      digitalWrite(BUILTIN_LED, state);
      lastMsg = millis();
      coinInserted = 1;
      coinValue++;
      debounce =1;
  }
}